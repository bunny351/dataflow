#### compiler-driver for "dataflow" language
#
# expects "PREFIX" to be set to the name of the installation prefix


set -e


function usage()
{
    echo "usage: dataflow [--here] [--keep] [OPTION ...] FILENAME [-o OUTFILENAME]"
    exit 1
}


test -z "$1" && usage

dflags="-x c"
infile=""
outfile="a.out"
binpath="$PREFIX/libexec/dataflow"
LIBS="-ldataflow -lm $LIBS"
keep=""

while test -n "$1"; do
    case "$1" in
	--here)
	    binpath="."
	    CFLAGS="$CFLAGS -I. -L."
	    shift;;
	--keep)
	    keep=1
	    shift;;
	-h|-help|--help)
	    usage;;
	-o) 
	    outfile="$2"
	    test -n "$2" || usage
	    shift
	    shift;;
	-*) 
	    CFLAGS="$CFLAGS $1"
	    shift;;
	*)
	    test -z "$infile" || usage
	    infile="$1"
	    shift;;
    esac
done

case "$outfile" in
    *.c)
	tmpfile=$(mktemp tmpXXXXXXXX)
	$binpath/preprocess -b <"$infile" >"$tmpfile"
	$binpath/pass1 -q <"$tmpfile" >"$outfile"
	$binpath/pass2 -q <"$tmpfile" >>"$outfile"
	$binpath/pass3 -q <"$tmpfile" >>"$outfile"
	test -n "$keep" || rm -f "$tmpfile";;
    *.i)
	tmpfile1=$(mktemp tmpXXXXXXXX)
	tmpfile2=$(mktemp tmpXXXXXXXX)
	$binpath/preprocess -b <"$infile" >"$tmpfile1"
	$binpath/pass1 -q <"$tmpfile1" >"$tmpfile2"
	$binpath/pass2 -q <"$tmpfile1" >>"$tmpfile2"
	$binpath/pass3 -q <"$tmpfile1" >>"$tmpfile2"
	gcc $dflags -E $CFLAGS "$tmpfile2" >"$outfile"
	test -n "$keep" || rm -f "$tmpfile1" "$tmpfile2";;
    *.o)
	tmpfile1=$(mktemp tmpXXXXXXXX)
	tmpfile2=$(mktemp tmpXXXXXXXX)
	$binpath/preprocess -b <"$infile" >"$tmpfile1"
	$binpath/pass1 -q <"$tmpfile1" >"$tmpfile2"
	$binpath/pass2 -q <"$tmpfile1" >>"$tmpfile2"
	$binpath/pass3 -q <"$tmpfile1" >>"$tmpfile2"
	gcc $dflags -c $CFLAGS "$tmpfile2" -o "$outfile"
	test -n "$keep" || rm -f "$tmpfile1" "$tmpfile2";;
    *)
	tmpfile1=$(mktemp tmpXXXXXXXX)
	tmpfile2=$(mktemp tmpXXXXXXXX)
	$binpath/preprocess -b <"$infile" >"$tmpfile1"
	$binpath/pass1 -q <"$tmpfile1" >"$tmpfile2"
	$binpath/pass2 -q <"$tmpfile1" >>"$tmpfile2"
	$binpath/pass3 -q <"$tmpfile1" >>"$tmpfile2"
	gcc $dflags $CFLAGS "$tmpfile2" -o "$outfile" $LIBS
	test -n "$keep" || rm -f "$tmpfile1" "$tmpfile2";;
esac
