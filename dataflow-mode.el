;;; dataflow-mode.el - emacs mode for "dataflow"

;; derived from meta2-mode which is
;; Derived from: snobol-mode.el (by Shae Erisson)

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; adapted from http://claystuart.blogspot.com/2012/09/a-snobol4-major-mode-for-emacs.html
;; who got it from http://emacs-fu.blogspot.com/2010/04/creating-custom-modes-easy-way-with.html

(require 'generic-x) ;; required

(define-generic-mode 'dataflow-mode
  (list "#")                                       ;; comments
  ;; keywords
  (list "if" "then" "else" "when" "unless" "array" "print" "continuously"
	"until" "while" "module" "use" "and" "or" "not")
  '(("\"[^\"]*'"  . font-lock-string-face))    ;; highlights strings
  '("\\.dataflow$")                                     ;; file endings
   nil                                             ;; other function calls
  "A mode for dataflow files"                       ;; doc string
)

(add-to-list 'auto-mode-alist '("\\.dataflow$" . dataflow-mode))

(provide 'dataflow-mode)
