/* dataflow language support code */


#ifndef DATAFLOW_H
#define DATAFLOW_H


#include <stdlib.h>


#define VALUE             double
#define LOCAL             static

#define DEFAULT_VALUE     0
#define MAX_RANK            7


struct variable_list
{
  struct variable *variable;
  struct variable_list *next;
};


struct variable
{
  char *name;
  int bound;
  VALUE value;
  int timestamp;
  int unbound_dependencies;
  struct variable_list *dependants;
  void (*compute)(struct variable *);
  void (*function)(VALUE, ...);	/* dummy, for expansion of REF */
};


struct queue
{
  struct variable_list *first, *last;
};


typedef struct { VALUE (*function)(VALUE); VALUE value; } FUNCTION1;
typedef struct { VALUE (*function)(VALUE, VALUE); VALUE value; } FUNCTION2;
typedef struct { VALUE (*function)(VALUE, VALUE, VALUE); VALUE value; } FUNCTION3;
typedef struct { VALUE (*function)(VALUE, VALUE, VALUE, VALUE); VALUE value; } FUNCTION4;
typedef struct { VALUE (*function)(VALUE, VALUE, VALUE, VALUE, VALUE); VALUE value; } FUNCTION5;
typedef struct { VALUE (*function)(VALUE, ...); VALUE value; } FUNCTIONn;


extern void modify(struct variable *var, VALUE val);
extern void modify_array(struct variable *var, int *indices, VALUE val);
extern VALUE array_ref(VALUE array, ...);
extern void init_variable(struct variable *v, char *name);
extern void add_dependency(struct variable *v, struct variable *d);
extern void remove_dependency(struct variable *v, struct variable *d);
extern VALUE print_string(char *str);
extern VALUE print_value(VALUE x);
extern VALUE print_begin();
extern VALUE print_end();
extern int make_array(struct variable *var, char *name, ...);
extern void make_string(struct variable *var, char *name, char *str);


#define IS_ARRAY(x)           isnan(x)

#define CURRENT_MODULE(id)    main ## _ ## id
#define USE_MODULE(name)      name ## _initialize()

#define DECLARE_FUNCTION1(name) \
  extern FUNCTION1 ___ ## name;
#define DECLARE_FUNCTION2(name) \
  extern FUNCTION2 ___ ## name;
#define DECLARE_FUNCTION3(name) \
  extern FUNCTION3 ___ ## name;
#define DECLARE_FUNCTION4(name) \
  extern FUNCTION4 ___ ## name;
#define DECLARE_FUNCTION5(name) \
  extern FUNCTION5 ___ ## name;
#define DECLARE_FUNCTIONn(name) \
  extern FUNCTIONn ___ ## name;

#define REF(name, iscall, ...) \
  __builtin_choose_expr(iscall, \
    __builtin_choose_expr(sizeof(___ ## name) == sizeof(FUNCTION1),		\
			  (___ ## name.function)(__VA_ARGS__),		\
			  array_ref(___ ## name.value, __VA_ARGS__)),	\
			  ___ ## name.value)

#define DECLARE_VARIABLE(name) \
  struct variable ___ ## name; \
  static void compute___ ## name(struct variable *);

#define DEFINE_ARRAY(name, ...) \
  make_array(&___ ## name, #name, __VA_ARGS__);

#define DEFINE_STRING(name, str)   make_string(&___ ## name, #name, str);

#define PUSH_INDEX(index)    is[ i++ ] = (int)(index);

#define BEGIN_DEPENDENCIES   void CURRENT_MODULE(initialize)() { struct variable *v;
#define END_DEPENDENCIES     }
#define DEPENDENCIES(name) \
  v = &___ ## name; \
  init_variable(v, #name); \
  v->compute = compute___ ## name;
#define DEPENDS(name)        add_dependency(v, &___ ## name);

#define DEFINE_FUNCTION1(name, x) \
  static VALUE fn___ ## name(VALUE); \
  FUNCTION1 ___ ## name = { fn___ ## name, 0 };	\
  static VALUE fn___ ## name(VALUE x)

#define DEFINE_FUNCTION2(name, x, y) \
  static VALUE fn___ ## name(VALUE, VALUE); \
  FUNCTION2 ___ ## name = { fn___ ## name, 0 };	\
  static VALUE fn___ ## name(VALUE x, VALUE y)

#define DEFINE_FUNCTION3(name, x, y, z) \
  static VALUE fn___ ## name(VALUE, VALUE, VALUE); \
  FUNCTION3 ___ ## name = { fn___ ## name, 0 };		\
  static VALUE fn___ ## name(VALUE x, VALUE y, VALUE z)

#define DEFINE_FUNCTION4(name, x, y, z, u)			\
  static VALUE fn___ ## name(VALUE, VALUE, VALUE, VALUE);	\
  FUNCTION4 ___ ## name = { fn___ ## name, 0 };		\
  static VALUE fn___ ## name(VALUE x, VALUE y, VALUE z, VALUE u)

#define DEFINE_FUNCTION5(name, x, y, z, u, v)			\
  static VALUE fn___ ## name(VALUE, VALUE, VALUE, VALUE, VALUE);	\
  FUNCTION5 ___ ## name = { fn___ ## name, 0 };		\
  static VALUE fn___ ## name(VALUE x, VALUE y, VALUE z, VALUE u, VALUE v)

#define DEFINE_FUNCTIONn(name, ...)	   \
  static VALUE fn___ ## name(__VA_ARGS__);	   \
  FUNCTIONn ___ ## name = { fn___ ## name, 0 };	   \
  static VALUE fn___ ## name(__VA_ARGS__)

#define DEFINE_VARIABLE(name) \
  void compute___ ## name(struct variable *v)

#define BEGIN_DEFINITION \
  { VALUE r; int f, i = 0; static void *step = NULL; int is[ MAX_RANK ];
#define END_DEFINITION \
  ;; done: \
  if(i) modify_array(v, is, r); else modify(v, r);}

#define BEGIN_COMPUTATION \
  if(step) goto *step; \
  for(f = 1; f--; r =
#define END_COMPUTATION            )

#define BEGIN_NEXT_STEP(label) \
  ;; step = &&label; goto done; label:;		\
  for(f = 1; f--; r =
#define END_NEXT_STEP(label)       )

#define BEGIN_ALTERNATIVE(label)   for(int label = 1; label--; ({r =
#define END_ALTERNATIVE(label)     ; goto done;}))

#define FAIL                       return;
#define DISABLE                    { remove_dependency(v, &___time); return; }


extern struct variable ___time, ___on, ___off, ___pi;

#ifdef GRAPHICS
extern struct variable ___mousex, ___mousey;
extern struct variable ___buttondown, ___buttonup, ___keydown, ___keyup;
extern struct variable ___keycode, ___key, ___quit;
extern struct variable ___frame;
#endif


DECLARE_FUNCTION1(exit)
DECLARE_FUNCTION2(modulo)
DECLARE_FUNCTION1(exit);
DECLARE_FUNCTION2(modulo);
DECLARE_FUNCTION1(sin);
DECLARE_FUNCTION1(cos);
DECLARE_FUNCTION1(tan);
DECLARE_FUNCTION1(asin);
DECLARE_FUNCTION1(acos);
DECLARE_FUNCTION1(atan);
DECLARE_FUNCTION1(log);
DECLARE_FUNCTION1(exp);
DECLARE_FUNCTION1(sqrt);
DECLARE_FUNCTION2(pow);
DECLARE_FUNCTION1(rnd);
DECLARE_FUNCTION1(put);
DECLARE_FUNCTION1(truncate);
DECLARE_FUNCTION1(frac);
DECLARE_FUNCTION1(round);
DECLARE_FUNCTION1(floor);
DECLARE_FUNCTION1(ceiling);
DECLARE_FUNCTION2(row_majow_ref);
DECLARE_FUNCTION1(size);
DECLARE_FUNCTION1(rank);
DECLARE_FUNCTION3(between);
DECLARE_FUNCTION1(frequency);
DECLARE_FUNCTION5(map);
DECLARE_FUNCTION3(norm);
DECLARE_FUNCTION3(lerp);
DECLARE_FUNCTION3(constrain);
DECLARE_FUNCTION2(min);
DECLARE_FUNCTION2(max);
DECLARE_FUNCTION4(distance);
DECLARE_FUNCTION1(sq);
DECLARE_FUNCTION1(abs);
DECLARE_FUNCTION1(sgn);
DECLARE_FUNCTION1(even);
DECLARE_FUNCTION1(odd);

#ifdef GRAPHICS
DECLARE_FUNCTION1(framerate);
DECLARE_FUNCTION3(screen);
DECLARE_FUNCTION1(foreground);
DECLARE_FUNCTION1(background);
DECLARE_FUNCTION2(pixel);
DECLARE_FUNCTION4(line);
DECLARE_FUNCTION4(rectangle);
DECLARE_FUNCTION3(polygon);
DECLARE_FUNCTION3(circle);
DECLARE_FUNCTION4(ellipse);
DECLARE_FUNCTION4(update);
DECLARE_FUNCTION3(rgb);
DECLARE_FUNCTION4(rgba);
DECLARE_FUNCTION1(antialiasing);
DECLARE_FUNCTION2(keyrepeat);
DECLARE_FUNCTION1(fill);
DECLARE_FUNCTION1(font);
DECLARE_FUNCTION2(load_font);
DECLARE_FUNCTION3(text);
DECLARE_FUNCTION3(glyph);
DECLARE_FUNCTION1(textwidth);
DECLARE_FUNCTION1(textheight);
DECLARE_FUNCTION1(load_image);
DECLARE_FUNCTION3(image);
DECLARE_FUNCTION1(image_zoom);
DECLARE_FUNCTION1(image_rotation);
DECLARE_FUNCTION1(save_image);
DECLARE_FUNCTION1(clear);
DECLARE_FUNCTION1(color);
#endif

#endif
