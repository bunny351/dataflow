include config.mk

PREFIX ?= $(HOME)
META2 = meta2
CC = gcc
DEBUG = 1
CFLAGSALL = -std=c99
CFLAGSLIB = $(CFLAGSALL) -I. -L. -Wall -W -Wno-unused-label \
	-Wno-unused-function -Wno-unused-parameter -Wno-empty-body
CFLAGS = $(CFLAGSALL)
CFLAGSWRK = $(CFLAGSLIB) -I$(HOME)/include
LIBS = -lm

ifneq ($(DEBUG),)
OPTFLAGS += -g
else
OPTFLAGS += -O2 -DNDEBUG
endif

ifneq ($(GRAPHICS),)
CFLAGSLIB += -DGRAPHICS $(shell sdl-config --cflags)
CFLAGS += -DGRAPHICS
LIBS += $(shell sdl-config --libs)
endif

ifneq ($(HAVE_SDL_IMAGE),)
CFLAGSLIB += -DHAVE_SDL_IMAGE
LIBS += -lSDL_image
endif

ifneq ($(HAVE_SDL_TTF),)
CFLAGSLIB += -DHAVE_SDL_TTF
LIBS += -lSDL_ttf
endif

ifneq ($(HAVE_SDL_GFX),)
CFLAGSLIB += -DHAVE_SDL_GFX
LIBS += -lSDL_gfx
endif


.PHONY: all clean check install


all: reference preprocess pass1 pass2 pass3 libdataflow.a dataflow


%.c: %.meta2
	meta2 -cq <$< >$@
%: %.c
	$(CC) $(CFLAGSWRK) $(OPTFLAGS) $< -o $@


preprocess: preprocess.meta2
reference: reference.meta2
pass1: pass1.meta2
pass2: pass2.meta2
pass3: pass3.meta2

clean:
	rm -f preprocess reference pass1 pass2 pass3
	rm -f preprocess.c reference.c pass1.c pass2.c pass3.c
	rm -f libdataflow.a dataflow.o
	rm -f dataflow

check: all fac
	./fac

fac.c: fac.dataflow preprocess pass1 pass2 pass3
	./preprocess -b <fac.dataflow >fac.dataflowp
	./pass1 -q <fac.dataflowp >fac.c
	./pass2 -q <fac.dataflowp >>fac.c
	./pass3 -q <fac.dataflowp >>fac.c

fac: fac.c dataflow.h libdataflow.a
	$(CC) $(CFLAGS) $(CFLAGSWRK) -E $< -o fac.i
	$(CC) $(CFLAGS) $(CFLAGSWRK) $^ -o $@ -ldataflow $(LIBS)

dataflow.o: dataflow.c dataflow.h WELL1024a.h
	$(CC) -c $(CFLAGSLIB) $(OPTFLAGS) $< -o $@

WELL1024a.o: WELL1024a.c WELL1024a.h
	$(CC) -c $(CFLAGSLIB) $(OPTFLAGS) $< -o $@

dataflow: dataflow.sh
	echo "#!/bin/bash" >$@
	echo 'PREFIX="'$(PREFIX)'"' >>$@
	echo 'CFLAGS="'$(CFLAGS)'"' >>$@
	echo 'LIBS="'$(LIBS)'"' >>$@
	cat $< >>$@
	chmod +x $@

libdataflow.a: dataflow.o WELL1024a.o
	ar cru $@ $^

install: all
	mkdir -p $(PREFIX)/bin
	mkdir -p $(PREFIX)/include
	mkdir -p $(PREFIX)/lib
	mkdir -p $(PREFIX)/libexec/dataflow
	install -m744 dataflow $(PREFIX)/bin
	install -m744 preprocess pass1 pass2 pass3 $(PREFIX)/libexec/dataflow
