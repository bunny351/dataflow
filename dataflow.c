/* dataflow language support code */


#define _ISOC_99_SOURCE

#include "dataflow.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include <stdint.h>
#include <math.h>
#include <time.h>

#include "WELL1024a.h"

#ifdef GRAPHICS
# include <SDL/SDL.h>
# ifdef HAVE_SDL_GFX
#  include <SDL/SDL_gfxPrimitives.h>
#  include <SDL/SDL_framerate.h>
#  include <SDL/SDL_rotozoom.h>
# endif

# define MAX_POINTS         1024

# ifdef HAVE_SDL_TTF
#  include <SDL/SDL_ttf.h>

#  define MAX_FONTS         256
# endif

# ifdef HAVE_SDL_IMAGE
#  include <SDL/SDL_image.h>

#  define MAX_IMAGES        1024
# endif
#endif


#define MAX_ARRAYS          256
#define ARRAY_VALUE         0x7ff8000000000000ULL
#define ARRAY_MASK          0x0000ffffffffffffULL


struct array {
  int rank;
  int dimensions[ MAX_RANK ];
  int total;
  VALUE *data;
};


static struct queue current_queue, next_queue;
static int timestamp, iteration, frequency;
static int columns;
static struct array array_table[ MAX_ARRAYS ];
static int arrays;
static struct variable_list *free_list;


extern void main_initialize();


#ifndef NDEBUG
# define d(fstr, ...)   fprintf(stderr, "%06d(%03d): " fstr, timestamp, iteration, __VA_ARGS__)
# define d2(...)        fprintf(stderr, __VA_ARGS__)
# define DEBUGGING      if(1)
#else
# define d(...)   
# define d2(...)
# define DEBUGGING      if(0)
#endif


#define TEST(x)         ((x) != 0)


struct variable ___time, ___on, ___off, ___pi;

#ifdef GRAPHICS
# define DEFAULT_FRAMERATE     60

static SDL_Surface *screen;
static SDL_Event event;
static Uint32 foreground_color, background_color;
static int use_antialiasing, fill_graphics;
static int framerate;
static double zoom, rotation;

struct variable ___frame;
struct variable ___mousex, ___mousey, ___buttondown, ___button;
struct variable ___keydown, ___keycode, ___key, ___quit;

# ifdef HAVE_SDL_GFX
static Sint16 xbuffer[ MAX_POINTS ];
static Sint16 ybuffer[ MAX_POINTS ];
static FPSmanager fpsmanager;
# endif

# ifdef HAVE_SDL_TTF
static int current_font;
static int font_counter;
static int last_text_width, last_text_height, last_text_index, last_text_string;
static TTF_Font *font_table[ MAX_FONTS ];
# endif

static SDL_Surface *image_table[ MAX_IMAGES ];
static int image_counter;
#endif


static void 
stop(char *msg, ...)
{
  va_list va;

  va_start(va, msg);
  fputs("Runtime error: ", stderr);
  vfprintf(stderr, msg, va);
  fputc('\n', stderr);
  exit(EXIT_FAILURE); 
}


static struct array *
find_array(VALUE a)
{
  /* XXX this is VALUE-type dependent */
  union nan { VALUE v; uint64_t n; } nan_value;

  if(!isnan(a)) stop("not an array: %g", a);

  nan_value.v = a;
  return &(array_table[ (nan_value.n & ARRAY_MASK) - 1 ]);
}


static struct variable_list *
make_variable_list_node(struct variable *var, struct variable_list *rest)
{
  struct variable_list *node;

  if(free_list == NULL) {
    node = (struct variable_list *)malloc(sizeof(struct variable_list));

    assert(node);
  }
  else {
    node = free_list;
    free_list = free_list->next;
  }

  node->variable = var;
  node->next = rest;
  return node;
}


void
release_variable_list_node(struct variable_list *node)
{
  node->next = free_list;
  free_list = node;
}


static void
enqueue(struct variable *v)
{
  if(v->timestamp > timestamp) return;	  /* already in queue or already executed in this iteration */
  else if(!next_queue.first)
    next_queue.first = next_queue.last = make_variable_list_node(v, NULL);
  else {
    next_queue.last->next = make_variable_list_node(v, NULL);
    next_queue.last = next_queue.last->next;
  }

  v->timestamp = timestamp + 1;
  d("\"%s\" enqueued [", v->name);
  DEBUGGING {
    struct variable_list *vl;

    for(vl = next_queue.first; vl != NULL; vl = vl->next)
      d2(" %s", vl->variable->name);

    d2(" ]\n");
  }
}


static void
swap_queues()
{
  current_queue.first = next_queue.first;
  current_queue.last = next_queue.last;
  next_queue.first = next_queue.last = NULL;
}


static void
trigger_dependants(struct variable_list *deps)
{
  while(deps != NULL) {
    struct variable *dvar = deps->variable;

    if(dvar->unbound_dependencies > 0) {
      if(--dvar->unbound_dependencies) 
	d("\"%s\" has now %d unbound dependenc%s\n", dvar->name, 
	  dvar->unbound_dependencies,
	  dvar->unbound_dependencies == 1 ? "y" : "ies");
    }

    enqueue(dvar);      
    deps = deps->next;
  }
}


void
modify(struct variable *var, VALUE val)
{
  int f = var->bound;

  var->bound = 1;

  if(!f || val != var->value) {
    var->value = val;
    d("\"%s\" <- %g\n", var->name, val);
    trigger_dependants(var->dependants);
  }
}


static void
peek_events()
{
  while(SDL_PollEvent(&event)) {
    switch(event.type) {
    case SDL_QUIT:
      modify(&___quit, 1);
      break;

    case SDL_KEYDOWN:
      modify(&___key, event.key.keysym.unicode);
      modify(&___keycode, event.key.keysym.sym);
      modify(&___keydown, 1);
      break;

    case SDL_KEYUP:
      modify(&___key, event.key.keysym.unicode);
      modify(&___keycode, event.key.keysym.sym);
      modify(&___keydown, 0);
      break;

    case SDL_MOUSEBUTTONUP:
      modify(&___button, event.button.button);
      modify(&___mousex, event.button.x);
      modify(&___mousey, event.button.y);
      modify(&___buttondown, 0);
      break;

    case SDL_MOUSEBUTTONDOWN:
      modify(&___button, event.button.button);
      modify(&___mousex, event.button.x);
      modify(&___mousey, event.button.y);
      modify(&___buttondown, 1);
      break;

    case SDL_MOUSEMOTION:
      modify(&___mousex, event.motion.x);
      modify(&___mousey, event.motion.y);
      break;
    }
  }
}


static void
process()
{
  int t0 = timestamp;

  iteration = -1;
  d2("000000(000): starting\n");

  do {
    struct variable_list *vl = current_queue.first;
    struct variable_list *vl2;
    struct variable *var;

    while(vl != NULL) {
      ++iteration;

      while(vl != NULL) {
	vl2 = vl->next; /* grab next before enqueueing modifies the 
			   next-ptr */
	var = vl->variable;

	if(var->timestamp > timestamp) var->timestamp = timestamp;

	if(var->unbound_dependencies == 0) {
	  if(var->compute) {
	    d("\"%s\"\n", var->name);
	    var->compute(var);   /* needs to call modify[_array]() if
				    value changes  */
	  }
	}
	else {
	  d("\"%s\" blocked (%d unbound dependencies)\n", var->name, 
	    var->unbound_dependencies);
	  enqueue(var);
	}

	release_variable_list_node(vl);
	vl = vl2;
      }

      swap_queues();
      vl = current_queue.first;	/* continue until queue is empty */
    }

    ++timestamp;
    modify(&___time, timestamp);
    swap_queues();

#ifdef GRAPHICS
    if(framerate && (timestamp - t0) > (frequency / framerate)) {
      t0 = timestamp;
      SDL_framerateDelay(&fpsmanager);
      modify(&___frame, ___frame.value + 1);
      SDL_Flip(screen);
      peek_events();
    }
  } while(1);
#else 
  } while(current_queue.first != NULL);
#endif
}


void
init_variable(struct variable *v, char *name)
{
  if(v->bound) return;

  v->name = name;
  v->bound = 0;
  v->value = DEFAULT_VALUE;
  v->timestamp = 0;
  v->dependants = NULL;
  v->unbound_dependencies = 0;
  v->compute = NULL;
  enqueue(v);
}

 
static void
init_bound_variable(struct variable *v, char *name)
{
  init_variable(v, name);
  v->bound = 1;
}


void 
add_dependency(struct variable *v, struct variable *d)
{
  struct variable_list *dl = d->dependants;
  struct variable_list *dl2;

  if(v == d) return;		/* ignore self-dependency */

  while(dl != NULL) {
    if(v == dl->variable) return;

    if(dl->next == NULL) break;
    else dl = dl->next;
  }

  dl2 = make_variable_list_node(v, NULL);

  if(dl == NULL) d->dependants = dl2;
  else dl->next = dl2;

  if(!d->bound) ++v->unbound_dependencies;

  dl2->variable = v;
  d2("000000(000): \"%s\" depends on \"%s\"\n", v->name, d->name);
}


void
remove_dependency(struct variable *v, struct variable *d)
{
  struct variable_list *dl = d->dependants;
  struct variable_list *dl2;

  if(v == d) return;		/* ignore self-dependency */

  dl2 = NULL;

  while(dl != NULL) {
    if(v == dl->variable) {
      if(dl2 == NULL) d->dependants = dl->next;
      else dl2->next = dl->next;

      release_variable_list_node(dl);

      if(!d->bound) --v->unbound_dependencies;

      d("removed dependency \"%s\" from \"%s\"\n", d->name, v->name);  
      return;
    }

    if(dl->next == NULL) break;
    else dl = dl->next;
  }
}


static void
run()
{
  struct variable_list *vl, *vl2;

  /* calculate all variables that have no dependencies */
  for(vl = current_queue.first; vl != NULL; vl = vl2) {
    struct variable *v = vl->variable;
    
    vl2 = vl->next;

    if(v->unbound_dependencies == 0) {
      d2("000000(000): \"%s\" has no dependencies\n", v->name);
      v->compute(v);
    }
    else enqueue(v);		/* otherwise add to initial queue */

    release_variable_list_node(vl);
  }      

  swap_queues();    /* 0-dependency vars have been removed */
  process();
}


VALUE
print_string(char *str)
{
  fputs(str, stdout);
  columns += strlen(str);
  return 1;
}


VALUE
print_value(VALUE x)
{
  static char buffer[ 256 ];

  if(IS_ARRAY(x)) sprintf(buffer, "<array:%d>", find_array(x)->rank);
  else sprintf(buffer, "%g", x);

  columns += strlen(buffer);
  fputs(buffer, stdout);
  return 1;
}


VALUE
print_begin()
{
  columns = 0;
  fflush(stderr);
  return 1;
}


VALUE
print_end()
{
  putchar('\n');
  fflush(stdout);
  return columns;
}


static VALUE 
array_count_to_value(int count)
{
  /* XXX this is VALUE-type dependent */
  union nan { VALUE v; uint64_t n; } nan_value;

  nan_value.n = ARRAY_VALUE + count;
  return nan_value.v;
}


int
make_array(struct variable *var, char *name, ...)
{
  va_list va;
  int total, i;
  struct array *a;
  
  assert(sizeof(VALUE) == 8);
  assert(arrays < MAX_ARRAYS);
  a = &(array_table[ arrays++ ]);
  va_start(va, name);
  total = 1;
  
  for(i = 0; i < MAX_RANK; ++i) {
    int d = va_arg(va, int);

    if(d == -1) break;

    a->dimensions[ i ] = d;
    total *= d;
  }

  va_end(va);
  a->rank = i;
  a->total = total;
  a->data = (VALUE *)malloc(sizeof(VALUE) * total);
  assert(a->data);

  for(i = 0; i < total; ++i)
    a->data[ i ] = 0;

  init_variable(var, name);
  var->bound = 1;
  var->value = array_count_to_value(arrays);
  return arrays - 1;
}


void
make_string(struct variable *var, char *name, char *str)
{
  int len = strlen(str);
  struct array *a = &(array_table[ make_array(var, name, len, -1) ]);
  VALUE *dptr = a->data;
  
  while(*str) 
    *(dptr++) = *(str++);
}


static char *
array_to_string(VALUE array)
{
  struct array *a = find_array(array);
  VALUE *dptr = a->data;
  char *buf = (char *)malloc(a->total + 1);
  char *bufptr = buf;
  int n = a->total;

  assert(buf);
  assert(a->rank == 1);
  
  while(n--) 
    *(bufptr++) = *(dptr++);

  *(bufptr++) = 0;
  return buf;
}


static unsigned short *
array_to_unicode(VALUE array)
{
  struct array *a = find_array(array);
  VALUE *dptr = a->data;
  unsigned short *buf = (unsigned short *)malloc(a->total + 1);
  unsigned short *bufptr = buf;
  int n = a->total;

  assert(buf);
  assert(a->rank == 1);
  
  while(n--) 
    *(bufptr++) = *(dptr++);

  *(bufptr++) = 0;
  return buf;
}


static VALUE *
array_location(VALUE array, int *indices)
{
  int i, d = 0;
  struct array *a;

  a = find_array(array);

  for(i = 0; i < a->rank; ++i) {
    VALUE j = indices[ i ];
    int k, d2;

    d2 = (int)j;

    if(d2 != j) stop("non-integral array index");

    if(d2 < 0 || d2 >= a->dimensions[ i ])
      stop("array index out of bounds"); /* XXX or ignore? */

    for(k = i + 1; k < a->rank; ++k)
      d2 = d2 * a->dimensions[ k ];

    d += d2;
  }

  return &(a->data[ d ]);
}


VALUE
array_ref(VALUE array, ...)
{
  va_list va;
  int is[ MAX_RANK ];
  int i;
  struct array *a = find_array(array);

  va_start(va, array);

  for(i = 0; i < a->rank; ++i)
    is[ i ] = va_arg(va, VALUE);

  va_end(va);
  return *array_location(array, is);
}


void
modify_array(struct variable *var, int *indices, VALUE val)
{
  VALUE *loc = array_location(var->value, indices); 

  *loc = val;
  d("\"%s(", var->name);
  DEBUGGING {
    struct array *a = find_array(var->value);
    int i;

    if(a->rank > 0) {
      d2("%d", indices[ 0 ]);

      for(i = 1; i < a->rank; ++i)
	d2(", %d", indices[ i ]);
    }

    d2(")\" <- %g\n", val);
  }

  trigger_dependants(var->dependants);
}


int
main(int argc, char *argv[])
{
  extern void init_dependencies();
  int i;
  unsigned int initrng[ 32 ];

  srand(time(0));
  
  for(i = 0; i < 32; ++i)
    initrng[ i ] = rand();

  InitWELLRNG1024a(initrng);
  free_list = NULL;
  arrays = 0;
  memset(array_table, 0, sizeof(struct array) * MAX_ARRAYS);
  timestamp = iteration = 0;
  frequency = 1000;
  init_bound_variable(&___time, "time");
  init_bound_variable(&___on, "on");
  ___on.value = 1;
  init_bound_variable(&___off, "off");
  init_bound_variable(&___pi, "pi");
  ___pi.value = M_PI;
  /* remove "time" from queue */
  next_queue.first = next_queue.last = NULL;
#ifdef GRAPHICS
  SDL_Init(SDL_INIT_EVERYTHING | SDL_INIT_NOPARACHUTE);
  SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
  SDL_EnableUNICODE(1);
# ifdef HAVE_SDL_TTF
  TTF_Init();
  current_font = -1;
  font_counter = 0;
  last_text_index = -1;
# endif
# ifdef HAVE_SDL_IMAGE
  image_counter = 0;
#endif
  foreground_color = 0xffffffff;
  background_color = 0x00000000;
  use_antialiasing = fill_graphics = 0;
  zoom = 1;
  rotation = 0;
  init_bound_variable(&___mousex, "mousex");
  init_bound_variable(&___mousey, "mousey");
  init_bound_variable(&___buttondown, "buttondown");
  init_bound_variable(&___button, "button");
  init_bound_variable(&___keydown, "keydown");
  init_bound_variable(&___keycode, "keycode");
  init_bound_variable(&___key, "key");
  init_bound_variable(&___quit, "quit");
  init_bound_variable(&___frame, "frame");
# ifdef HAVE_SDL_GFX
  SDL_initFramerate(&fpsmanager);
  framerate = DEFAULT_FRAMERATE;
  SDL_setFramerate(&fpsmanager, DEFAULT_FRAMERATE);
# endif
#endif
  main_initialize();
  run();  
  return 0;
}


DEFINE_FUNCTION1(exit, code) { exit(code); }
DEFINE_FUNCTION2(modulo, x, y) { return fmod(x, y); }
DEFINE_FUNCTION1(sin, x) { return sin(x); }
DEFINE_FUNCTION1(cos, x) { return cos(x); }
DEFINE_FUNCTION1(tan, x) { return tan(x); }
DEFINE_FUNCTION1(asin, x) { return asin(x); }
DEFINE_FUNCTION1(acos, x) { return acos(x); }
DEFINE_FUNCTION1(atan, x) { return atan(x); }
DEFINE_FUNCTION1(log, x) { return log(x); }
DEFINE_FUNCTION1(exp, x) { return exp(x); }
DEFINE_FUNCTION1(sqrt, x) { return sqrt(x); }
DEFINE_FUNCTION2(pow, x, y) { return pow(x, y); }
DEFINE_FUNCTION1(abs, x) { return x < 0 ? -x : x; }
DEFINE_FUNCTION1(sgn, x) { return x < 0 ? -1 : (x > 0 ? 1 : 0); }

DEFINE_FUNCTION1(rnd, x)
{
  return WELLRNG1024a() * x;
}

DEFINE_FUNCTION1(put, x) { putchar(x); return x; }
DEFINE_FUNCTION1(truncate, x) { return trunc(x); }
DEFINE_FUNCTION1(frac, x) { double i; return modf(x, &i); }
DEFINE_FUNCTION1(round, x) { return round(x); }
DEFINE_FUNCTION1(floor, x) { return floor(x); }
DEFINE_FUNCTION1(ceiling, x) { return ceil(x); }

DEFINE_FUNCTION2(row_major_ref, array, i)
 {
  struct array *a = find_array(array);
  int p = (int)i - 1;

  if(p >= 0 && p < a->total)
    stop("array index out of bounds");

  return a->data[ p ]; 
}

DEFINE_FUNCTION1(size, a) { return find_array(a)->total; }
DEFINE_FUNCTION1(rank, a) { return find_array(a)->rank; }
DEFINE_FUNCTION3(between, x, y, z) { return x <= y && y <= z; }
DEFINE_FUNCTION1(frequency, hz) { return frequency = hz; }

/* some functions found in "Processing" */
DEFINE_FUNCTION5(map, v, min1, max1, min2, max2)
{
  return (v - min1) / (max1 - min1) * (max2 - min2) + min2;
}

DEFINE_FUNCTION3(norm, v, min1, max1) { return (v - min1) / (max1 - min1); }
DEFINE_FUNCTION3(lerp, start, stop, amt) { return start + (amt * (stop - start)); }
DEFINE_FUNCTION3(constrain, x, lo, hi) { return x < lo ? lo : (x > hi ? hi : x); }
DEFINE_FUNCTION2(min, x, y) { return x < y ? x : y; }
DEFINE_FUNCTION2(max, x, y) { return x > y ? x : y; }
DEFINE_FUNCTION1(sq, x) { return x * x; }
DEFINE_FUNCTION1(even, x) { return x / 2 == 0; }
DEFINE_FUNCTION1(odd, x) { return x / 2 != 0; }

DEFINE_FUNCTION4(distance, x1, y1, x2, y2) 
{
  VALUE n1 = x2 - x1, n2 = y2 - y1;

  return sqrt(n1 * n1 + n2 * n2);
}


#ifdef GRAPHICS
DEFINE_FUNCTION1(framerate, hz) 
{
  int old = 0;

  /* has no effect if SDL_gfx is not available */
# ifdef HAVE_SDL_GFX
  old = framerate;
  SDL_setFramerate(&fpsmanager, framerate = (int)hz); 
# endif
  return old;
}

DEFINE_FUNCTION3(screen, w, h, d)
{
  screen = SDL_SetVideoMode(w, h, d, SDL_SWSURFACE);
  return screen != NULL;
}

DEFINE_FUNCTION1(foreground, col) { return foreground_color = col; }
DEFINE_FUNCTION1(background, col) { return background_color = col; }

DEFINE_FUNCTION3(rgb, r, g, b)
{
  return ((int)(r * 255) << 24) | ((int)(g * 255) << 16) | 
    ((int)(b * 255) << 8) | 255;
}

DEFINE_FUNCTION4(rgba, r, g, b, a)
{
  return ((int)(r * 255) << 24) | ((int)(g * 255) << 16) |
    ((int)(b * 255) << 8) | (int)(a * 255);
}

DEFINE_FUNCTION2(pixel, x, y)
{
# ifdef HAVE_SDL_GFX
  pixelColor(screen, x, y, foreground_color);
# endif
  return 1;
}

DEFINE_FUNCTION4(line, x1, y1, x2, y2)
{
# ifdef HAVE_SDL_GFX
  if(TEST(use_antialiasing))
    aalineColor(screen, x1, y1, x2, y2, foreground_color);
  else
    lineColor(screen, x1, y1, x2, y2, foreground_color);
# endif
  return 1;
}

DEFINE_FUNCTION4(rectangle, x, y, w, h)
{
# ifdef HAVE_SDL_GFX
  if(TEST(fill_graphics))
    boxColor(screen, x, y, x + w, y + w, background_color);

  if(!TEST(fill_graphics) || foreground_color != background_color)
    rectangleColor(screen, x, y, x + w, y + h, foreground_color);
# endif
  return 1;
}

# ifdef HAVE_SDL_GFX
static int 
convert_point_array(VALUE array, int start, int end)
{
  int i, len = end - start + 1;
  struct array *a = find_array(array);

  assert(len < MAX_POINTS);
  assert(len <= a->total);
  assert(a->total % 2 == 0);
  
  for(i = 0; i < a->total; i += 2) {
    xbuffer[ i / 2 ] = a->data[ i + start ];
    ybuffer[ i / 2 ] = a->data[ i + start + 1 ];
  }

  return len;
}
# endif

DEFINE_FUNCTION3(polygon, array, start, end)
{
# ifdef HAVE_SDL_GFX
  int len = convert_point_array(array, start, end);

  if(TEST(fill_graphics))
    filledPolygonColor(screen, xbuffer, ybuffer, len, background_color);

  if(!TEST(fill_graphics) || foreground_color != background_color) {
    if(TEST(use_antialiasing))
      aapolygonColor(screen, xbuffer, ybuffer, len, foreground_color);
    else
      polygonColor(screen, xbuffer, ybuffer, len, foreground_color);
  }
#endif

  return 1;
}

DEFINE_FUNCTION3(circle, x, y, r)
{
# ifdef HAVE_SDL_GFX
  if(TEST(fill_graphics))
    filledCircleColor(screen, x, y, r, background_color);

  if(!TEST(fill_graphics) || foreground_color != background_color) {
    if(TEST(use_antialiasing))
      aacircleColor(screen, x, y, r, foreground_color);
    else
      circleColor(screen, x, y, r, foreground_color);
  }
# endif

  return 1;
}

DEFINE_FUNCTION4(ellipse, x, y, w, h)
{
# ifdef HAVE_SDL_GFX
  if(TEST(fill_graphics))
    filledEllipseColor(screen, x, y, x + w, y + h, background_color);

  if(!TEST(fill_graphics) || foreground_color != background_color) {
    if(TEST(use_antialiasing))
      aaellipseColor(screen, x, y, x + w, y + h, foreground_color);
    else
      ellipseColor(screen, x, y, x + w, y + h, foreground_color);
  }
# endif

  return 1;
}

DEFINE_FUNCTION1(antialiasing, f) {
  int old = use_antialiasing;

  use_antialiasing = TEST(f);
  return old;
}

DEFINE_FUNCTION1(fill, f) { 
  int old = fill_graphics;

  fill_graphics = TEST(f);
  return old;
}

DEFINE_FUNCTION4(update, x, y, w, h)
{
  SDL_UpdateRect(screen, x, y, w, h);
  return 1;
}

DEFINE_FUNCTION2(keyrepeat, delay, interval) 
{
  SDL_EnableKeyRepeat(delay, interval);
  return 1;
}

DEFINE_FUNCTION1(font, index)
{
# ifdef HAVE_SDL_TTF
  int old = current_font;

  assert(index < font_counter);
  current_font = index;
  return old;
# else
  return 0;
# endif
}

DEFINE_FUNCTION2(load_font, name, size)
{
# ifdef HAVE_SDL_TTF
  TTF_Font *fontptr;
  char *fname = array_to_string(name);

  assert(font_counter < MAX_FONTS - 1);
  fontptr = TTF_OpenFont(fname, size);
  
  if(!fontptr)
    stop("can not load font \"%s\" (size %d)", fname, (int)size);

  free(fname);
  font_table[ font_counter ] = fontptr;
  return font_counter++;
# else
  return 0;
# endif
}

DEFINE_FUNCTION1(textwidth, str)
{
# ifdef HAVE_SDL_TTF
  unsigned short *text;

  if(last_text_index >= 0 && last_text_string == str)
    return last_text_width;

  text = array_to_unicode(str);
  TTF_SizeUNICODE(font_table[ current_font ], text, &last_text_width, &last_text_height);
  free(text);
  return last_text_width;
# else
  return 0;
# endif
}

DEFINE_FUNCTION1(textheight, str)
{
# ifdef HAVE_SDL_TTF
  unsigned short *text;

  if(last_text_index >= 0 && last_text_string == str)
    return last_text_height;

  text = array_to_unicode(str);
  TTF_SizeUNICODE(font_table[ current_font ], text, &last_text_width, &last_text_height);
  free(text);
  return last_text_height;
# else
  return 0;
# endif
}

DEFINE_FUNCTION3(text, str, x, y)
{
# ifdef HAVE_SDL_TTF
  unsigned short *text;
  SDL_Color col;
  TTF_Font *font = font_table[ current_font ];
  SDL_Surface *rtxt;
  SDL_Rect dest;

  text = array_to_unicode(str);
  col.r = (foreground_color >> 24) & 255;
  col.g = (foreground_color >> 16) & 255;
  col.b = (foreground_color >> 8) & 255;
  dest.x = x;
  dest.y = y;
  
  if(TEST(use_antialiasing))
    rtxt = TTF_RenderUNICODE_Blended(font, text, col);
  else
    rtxt = TTF_RenderUNICODE_Solid(font, text, col);

  if(rtxt) {
    SDL_BlitSurface(rtxt, NULL, screen, &dest);
    SDL_FreeSurface(rtxt);
  }

  free(text);
# endif
  return 1;
}

DEFINE_FUNCTION3(glyph, chr, x, y)
{
# ifdef HAVE_SDL_TTF
  SDL_Color col;
  TTF_Font *font = font_table[ current_font ];
  SDL_Surface *rtxt;
  SDL_Rect dest;

  col.r = (foreground_color >> 24) & 255;
  col.g = (foreground_color >> 16) & 255;
  col.b = (foreground_color >> 8) & 255;
  dest.x = x;
  dest.y = y;
  rtxt = TTF_RenderGlyph_Solid(font, chr, col);

  if(rtxt) {
    SDL_BlitSurface(rtxt, NULL, screen, &dest);
    SDL_FreeSurface(rtxt);
  }

# endif
  return 1;
}

DEFINE_FUNCTION1(load_image, name)
{
  SDL_Surface *sptr;
  char *fname = array_to_string(name);

  assert(image_counter < MAX_IMAGES - 1);
# ifdef HAVE_SDL_IMAGE
  sptr = IMG_Load(fname);
# else
  sptr = SDL_LoadBMP(fname);
# endif

  if(!sptr)
    stop("can not load image \"%s\"", fname);

  image_table[ image_counter ] = SDL_DisplayFormat(sptr);
  SDL_FreeSurface(sptr);
  return image_counter++;
}

DEFINE_FUNCTION3(image, img, x, y) /* position of image center! */
{
  SDL_Rect rect;
  SDL_Surface *pic, *pic2;

  assert(img < image_counter);
  pic2 = pic = image_table[ (int)img ];

#ifdef HAVE_SDL_GFX
  if(rotation != 0)
    pic2 = rotozoomSurface(pic, rotation, zoom, use_antialiasing);
  else
    pic2 = zoomSurface(pic, zoom, zoom, use_antialiasing);
#endif

  rect.x = x - pic2->w / 2;
  rect.y = y - pic2->h / 2;
  SDL_BlitSurface(pic2, NULL, screen, &rect);

  if(pic != pic2) SDL_FreeSurface(pic2);

  return 1;
}

DEFINE_FUNCTION1(save_image, name)
{
  char *fname = array_to_string(name);

  return SDL_SaveBMP(screen, fname) == 0;
}

DEFINE_FUNCTION1(image_zoom, x)
{
  double old = zoom;

  zoom = x;
  return old;
}

DEFINE_FUNCTION1(image_rotation, r)
{
  double old = rotation;

  rotation = r;
  return old;
}

DEFINE_FUNCTION1(clear, color)
{
  Uint32 col = (Uint32)color;
  Uint32 c = SDL_MapRGB(screen->format, (col >> 24) & 255, (col >> 16) & 255,
			(col >> 8) & 255);
  SDL_Rect r;

  r.x = r.y = 0;
  r.w = screen->w;
  r.h = screen->h;
  SDL_FillRect(screen, &r, c);
  return 1;
}

DEFINE_FUNCTION1(color, col)
{
  foreground_color = background_color = col;
  return col;
}
#endif
